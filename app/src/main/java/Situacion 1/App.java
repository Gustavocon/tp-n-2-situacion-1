package situacion;

public class App // Clase encargada de instanciar objetos, enviar mensajes e imprimir por pantalla los resultados
{
    public static void main(String[] args)
    {
        Alimento nuevoAlimento = new Alimento();
        nuevoAlimento.setInformacion("Banana","Es un fruto comestible, de varios tipos de grandes plantas herbaceas del genero Musa");
        nuevoAlimento.setContenidoVitaminas('B');
        nuevoAlimento.setContenidoPorcentaje(80,40,20);
        nuevoAlimento.setOrigenAnimal(false);

        InfoAlimento infoAlimento = new InfoAlimento();
        infoAlimento.mostrarInformacion(nuevoAlimento);

        System.out.print("\nEl alimento " + nuevoAlimento.getNombre());
        if (infoAlimento.esDietetico(nuevoAlimento) == false)
        {
            System.out.print(" no");
        }
        System.out.print(" es dietetico y ");

        if (infoAlimento.esRecomendableDeportistas(nuevoAlimento) == false)
        {
            System.out.print("no ");
        }
        System.out.println("es recomendable para deportistas");

        System.out.println("\nSu valor energetico es: " + infoAlimento.valorEnergetico(nuevoAlimento) + " kcal");
    }
}